import React from 'react';
import {useEffect, useState} from 'react';
import './App.css';

function App() {
    const [data, setData] = useState(null);
    useEffect(() => {
        (async () => {
            const rawData = await fetch('/data', {headers: {Accept: 'application/json'}});
            setData(await rawData.text());
        })();
    });

    return (
        <div className="App">
            <span>Backend data: {data}</span>
        </div>
    );
}

export default App;
